<?php

$_user_ = 'customer';
$_password_ = 'guest';

session_start();

$url_action = (empty($_REQUEST['action'])) ? 'logIn' : $_REQUEST['action'];
$auth_realm = (isset($auth_realm)) ? $auth_realm : '';

if (isset($url_action)) {
    if (is_callable($url_action)) {
        call_user_func($url_action);
    } else {
        echo 'Function does not exist, request terminated';
    };
};

function logIn() {
    global $auth_realm;

    if (!isset($_SESSION['username'])) {
        if (!isset($_SESSION['login'])) {
            $_SESSION['login'] = TRUE;
            header('WWW-Authenticate: Basic realm="'.$auth_realm.'"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'You must enter a valid order number and email address';
            echo '<p><a href="?action=logOut">Try again</a></p>';
            exit;
        } else {
            $orderID = isset($_POST['orderID']) ? $_POST['orderID'] : '';
            $email = isset($_POST['email']) ? $_POST['email'] : '';
            $result = authenticate($orderID, $email);
            if ($result) {
                $_SESSION['username'] = $result;
            } else {
                session_unset($_SESSION['login']);
                errMes($result);
                echo '<p><a href="">Try again</a></p>';
                exit;
            };
        };
    };
}

function authenticate($orderID,$email) { 
	if (isset($orderID) && isset($email)) { 
		// authenticate customer login from combination of order ID and email used to place order.
		require($_SERVER['DOCUMENT_ROOT'].'/includes/db-login.php');
		$q = "SELECT customer_email,customer_name FROM orders WHERE (id='".$orderID."') OR (transaction_id='".$orderID."');";
		$r = mysqli_query($db,$q) or die($q);
		$orderInfo = mysqli_fetch_array($r);
		
		if ($email == $orderInfo['customer_email']) { return 0; } 
		else { return $orderInfo['customer_name']; } 
	
		mysqli_close($db);
	} else { 
		return false;
	}
} 

function errMes($errno) {
    switch ($errno) {
        case 0:
            break;
        case 1:
            echo 'The order number or email address you entered is incorrect';
            break;
        default:
            echo 'Unknown error';
    };
}

function logOut() {

    session_destroy();
    if (isset($_SESSION['username'])) {
        session_unset($_SESSION['username']);
        echo "You've successfully logged out<br>";
        echo '<p><a href="?action=logIn">LogIn</a></p>';
    } else {
        header("Location: ?action=logIn", TRUE, 301);
    };
    if (isset($_SESSION['login'])) { session_unset($_SESSION['login']); };
    exit;
}

?>