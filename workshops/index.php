<?php $pageTitle = "XNAT Workshop Registration Dashboard"; ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/template/header.php'); ?>
<!-- page content goes here -->

<div class="container hidden">
<?php 
printArray($_GET);

function printArray($array){
	echo "<ul>";
	 foreach ($array as $key => $value){
		echo "<li>$key => $value</li>";
		if(is_array($value)){ //If $value is an array, print it as well!
			printArray($value);
		}  
	} 
	echo "</ul>";
}

?>
</div>


<?php /* SHOW ALL ORDERS */
	// get all orders
	$q = "SELECT *,DATE(timestamp) as order_date FROM registrations WHERE (status != 'incomplete') AND (status != 'failed') AND (status != 'refund') order by order_date DESC";
	$order_list = mysqli_query($db,$q) or die($q);

      $q = "SELECT sum(registration_fee) as budget, count(registration_fee) as attendees FROM registrations WHERE status='complete'";
      $summary = mysqli_fetch_array(mysqli_query($db,$q)) or die($q);
?>

<p>Attendees: <?php echo $summary['attendees'] ?>  |  Registration Fees: $ <?php echo number_format($summary['budget'],2) ?></p>

    <table class="table table-condensed table-hover" id="orderTable">
      <thead>
        <tr>
          <th>Order ID</th>
          <th>Customer</th>
          <th>Email</th>
            <th>Institution</th>
          <th>Registration Date</th>
          <th>Student Discount?</th>
            <th>Comments</th>
        </tr>
      </thead>
      <tbody>
      	<?php 
		if ($order_list) : 
		  while ($order = mysqli_fetch_array($order_list)) : 
              ?>
          <tr>
          	  <td id="<?php echo $order['ID'] ?>">
            	<a href="/workshops/view-registration.php?orderNum=<?php echo $order['ID'] ?>"><?php echo $order['ID'] ?></a></td>
              <td><?php echo $order['name'] ?></td>
              <td><?php echo $order['contact_email'] ?></td>
              <td><?php echo $order['institution'] ?></td>
              <td><?php echo $order['order_date'] ?></td>
              <td align="center"><?php if ($order['discount']) echo '<span class="glyphicon glyphicon-ok"></span>' ?></td>
              <td align="center"><?php if ($order['comments']) echo '<span class="glyphicon glyphicon-comment" data-toggle="tooltip" data-placement="top" title="'. $order['comments'] .'"></span>' ?></td>
          </tr>
		  <?
		  endwhile;
        else :
            ?>
            <tr>
                <td colspan="7">No completed orders found in database.</td>
            </tr>
        <?php
		endif;  
		?>
      </tbody>  
      <tfoot>
      	<tr>
          <th>Quick Filter:</th>
          <th>Customer</th>
          <th>Order Date</th>
          <th>Institution</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </tfoot>
    </table>
    
    <div class="hidden" id="show-all">
    	<a href="/workshops/index.php">Show All Orders</a>
    </div>
    
    <div class="hidden">
        <div id="set-session-var"></div>
    </div>
    
<!--    <script src="/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script> -->
    <script>
	/* Initialize table and controls based on whether we show all records or not */ 
	var asInitVals = new Array();
 	

	$(document).ready(function() {
		// initialize data table
		var oTable = $('#orderTable').DataTable( {
			"iDisplayLength": 100,
            "order": [[0,'desc']]
		} );
		
		// after table is created, add bootstrap standard classes to controls. 
		$(".dataTables_filter label input").addClass('form-control'); 
		$(".dataTables_length label select").addClass('form-control');
		
		$("#filter-status").on( "change",function(){ 
			statusFilter( $(this).val(),'#orderTable','dashboardSelected');
		});

        /* if session remembers a previously stored filter setting, apply that now */
        var sessionFilter = "<?php echo $_SESSION['dashboardSelected'] ?>";
        if (sessionFilter.length > 0) {
            statusFilter(sessionFilter,'#orderTable','dashboardSelected');
        }
		
	} );	
	
	// HACK: move table filter menu into header, after table has been fully initialized
	$('#orderTable').on("init.dt",function(){ 
		console.log("complete!");
		$('#orderTable_filter').append( $('#filter-status-container') );
		// statusFilter("OPEN",'#orderTable','dashboardSelected');
	});

  // Initialize Tooltips
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
	</script>



<!-- end page content -->
<?php require($_SERVER['DOCUMENT_ROOT'] . '/template/footer.php'); ?>