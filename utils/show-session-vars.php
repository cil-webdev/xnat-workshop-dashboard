<?php $pageTitle = "Update Database"; ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/header.php'); ?>
<!-- page content goes here -->

<?php
printArray($_SESSION);

function printArray($array){
    echo "<ul>";
    foreach ($array as $key => $value){
        echo "<li>$key => $value</li>";
        if(is_array($value)){ //If $value is an array, print it as well!
            printArray($value);
        }
    }
    echo "</ul>";
}

?>

<!-- end page content -->
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/footer.php'); ?>