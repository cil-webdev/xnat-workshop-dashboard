/* initialize multi-pane shipping code entry */
/* $(document).ready(function() {
	$('#usps1, #usps2, #usps3').autotab_magic().autotab_filter('numeric');
}); */

/* on-click functions for shipping status */
function shipThis() {
	$('.shipping').addClass('required').each(function(){
		$(this).prev('label').append(' <strong>Required</strong>');
	});
}
function openThis() {
	$('.tx').html('<label for="tx">CashNet transaction ID:</label> <input type="text" name="tx" size="10" class="required" />');	
}
function cancelThis() {
	if (confirm("Are you sure you want to cancel this order?")) { 
		$('#updateStatusForm').submit(); 
	}
}
function refundThis() {
	$('.refund').addClass('required').each(function(){
		$(this).prev('label').append(' <strong>Required</strong>');
	});	
}

/* ORDER FUNCTIONS */

$(document).ready(function(){
    // populate EDIT DRIVE modal menu with options specific to that drive order
    $(".drive-select").on("click",function(){
        var release = $(this).data("release");
        var hiddenSelect = $("select[name=serial-hidden]");
        var userSelect = $("select[name=serial]");
        // remove all options from user-selectable menu, except the select header
        $(userSelect).find("option").not(".select-header").each(function(){
            $(this).appendTo($(hiddenSelect)) // move all options to hidden select for evaluation
        });
        $(hiddenSelect).find("option").each(function(){
            console.log($(this).html(), release, $(this).html().indexOf(release));
            if ($(this).html().indexOf(release) > -1) {
                $(this).appendTo($(userSelect));
            }
        });
        $('#addDriveToOrderForm input[name=release_id]').val(release); // add data release to modal title.

        // if a serial already exists, select it in the menu
        var serial = $(this).data("serial");
        if (serial) {
            $("select[name=serial]").find("option[value="+serial+"]").prop("selected","selected");
        }
    });

    // determine which status is being selected, then configure the status modal accordingly.
    $('#updateStatusButton').on("click",function(){
        var status = $('#order-status').val();
        updateOrder(status);
    });
});

function updateOrder(status) {
    var status = (status) ? status : $('#order-status').val();
    var statusClass = status.replace(/ /g,"-");
    $('#updateModalTitle').html(status);
    $('#updateStatusModal input[name=status]').val(status);
    $('#updateStatusModal .form-group').addClass("hidden").find('input').prop("disabled","disabled"); // hide & disable all options
    $('#updateStatusModal').find('.'+statusClass).removeClass("hidden").find('input').prop("disabled",false); // show only appropriate options
}


/* DRIVE FUNCTIONS */




function validateForm(id) { 
	$(id+' .required').each(function() {
        // if required fields are empty, throw an alert and prevent form submission.
        if ($(this).val().length == 0) {
            $(id).parents('.modal-body').prepend('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Required field missing:</strong> ' + $(this).prop('name') + '</div>');
            return false;
        }
    });

    var driveStatus = $(id).find('select[name=drive_status]').find('option:selected').val();
    if (driveStatus == 'returned to HCP') {
        confirm("This drive was originally shipped to a customer and has been returned?");
    }
    // if script does not return false, submit form.

    // remove spaces from Fedex tracking code in input
    var trackingCode = $(id).find('input[name=tracking_code]').val();
    if(trackingCode) {
        trackingCode = trackingCode.replace(/\s+/g, '');
        $(id).find('input[name=tracking_code]').val(trackingCode);
    }

    // submit form
    $(id).submit();
} 
function editDrive(el) {
	// set modal title 
	$('#editModalLabel').html('Edit Drive Info'); 
	
	// unset required fields, if necessary. 
	$('.form-group').removeClass('required');

	// read values from data table, insert into form fields in modal
    $('#driveEditModal #input-serial').val( $(el).find('span[name="serial"]').html() );
    $('#driveEditModal input[name=serial]').val( $(el).find('span[name="serial"]').html() );
	$('#driveEditModal #input-drive_type').val( $(el).find('span[name="drive_type"]').html() );
	$('#driveEditModal #input-drive_size').val( $(el).find('span[name="drive_size"]').html() );
	$('#driveEditModal #input-drive_cost').val( $(el).find('span[name="drive_cost"]').html() );
	$('#driveEditModal #input-release_version').val( $(el).find('span[name="release_version"]').html() );
	$('#driveEditModal #input-drive_format').val( $(el).find('span[name="drive_format"]').html() );
    $('#driveEditModal input[name=prev_status]').val( $(el).find('span[name="drive_status"]').html() );
	
	var driveStatus = $(el).find('span[name="drive_status"]').html();
	$('#driveEditModal #select-drive_status option[value="'+driveStatus+'"]').prop("selected","selected");
	
	var releaseId = $(el).find('span[name="release_id"]').html();
	$('#driveEditModal #select-release_id option[value="'+releaseId+'"]').prop("selected","selected");
	
	// set drive form action
	$('#driveFormAction').val('modify_drive');
} 

function newDrive() { 
	// set modal title 
	$('#editModalLabel').html('Add New Drive'); 
	
	//reset values
	$('#driveEditModal .form-control').val('');
	$('#driveEditModal #select-drive_status option[value="new"]').prop("selected","selected");
	$('#driveEditModal #select-release_id option').prop("selected",false);
	
	// add required fields
	$('#input-serial').parents('.form-group').addClass('required');
	$('#select-drive_status').parents('.form-group').addClass('required');
	
	// set drive form action
	$('#driveFormAction').val('new_drive');
}

function statusFilter(opt,table,param){
    console.log("filtered on",opt);
    $(table).DataTable().column(6).search(opt).draw();
    $('#filter-status option[value="'+opt+'"]').prop("selected","selected"); // go back and make sure the filter menu is set if we call this function from inside the code.
    $('#set-session-var').load('/utils/set-session-var.php?'+param+'='+opt);
}