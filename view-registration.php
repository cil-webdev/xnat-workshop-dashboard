<?php $pageTitle = "Registration Dashboard"; ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/header.php'); ?>
    <!-- page content goes here -->

    <div class="container hidden">
        <?php
        printArray($_GET);

        function printArray($array){
            echo "<ul>";
            foreach ($array as $key => $value){
                echo "<li>$key => $value</li>";
                if(is_array($value)){ //If $value is an array, print it as well!
                    printArray($value);
                }
            }
            echo "</ul>";
        }

        ?>
    </div>

<?php /* SHOW ONE ORDER */
if (isset($_GET['orderNum'])) :
    $orderNum = $_GET['orderNum'];
    // get this order
    $q = "SELECT *,DATE(timestamp) as order_date FROM registrations WHERE ID='".$_GET['orderNum']."';";
    $order = mysqli_fetch_array(mysqli_query($db,$q));
    ?>

    <script>
        $(document).ready(function(){
            $('#page-title h1').html('Registration # <?php echo $order['ID'] ?>: <?php echo $order['name'] ?>');
        });
    </script>

    <div class="row">
        <div class="col-md-4">
            <!-- Order metadata -->
            <h3>Registration Information</h3>
            <p><strong>Attendee Information</strong>  <button class="btn btn-default btn-xs pull-right" data-toggle="modal" data-target="#editContact">Edit</button></p>
            <div class="well">
                <ul style="list-style:none; margin:0; padding:0">
                    <?php
                    $participantKeys = array('Name' => 'contact_name','Contact Email' => 'contact_email','Contact Phone'=>'contact_phone','Institution'=>'institution');
                    foreach( $participantKeys as $k => $v):
                        if ($order[$v]) echo "<li>".$k.": ".$order[$v]."</li>";
                    endforeach;
                    ?>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <!-- Order receipt -->
            <h3>Order Receipt</h3>

            <table class="table table-condensed table-striped">
                <tbody>
                <tr style="font-weight:bold">
                    <td>CashNet Transaction ID</td>
                    <td><?php echo $order['transaction_id'] ?></td>
                </tr>
                <tr>
                    <td>Total Registration Cost: </td>
                    <td>$ <?php echo number_format($order['registration_fee'],2) ?></td>
                </tr>
                <tr>
                    <td>CashNet Fee: </td>
                    <td>$ <?php echo number_format($order['cashnet_fee'],2) ?></td>
                </tr>
                <tr>
                    <td>Customer Notes: </td>
                    <td><em><?php echo $order['comments'] ?></em></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <!-- Order status -->
            <h3>Registration Status</h3>
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-6 control-label">Status</label>
                    <div class="col-sm-6">
                        <select name="order-status" id="order-status" class="form-control">
                            <option value="">Select</option>
                            <?php foreach ($registration_status as $key => $value) : ?>
                                <option value="<?php echo $value ?>" <?php echo ($order['status'] == $value) ? "selected" : "" ?>><?php echo strtoupper($value) ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <table class="table table-condensed table-striped">
                    <tbody>
                    <?php
                    // switch rows based on order status
                    switch ($order['status']) {
                        case "complete": ?>
                            <tr><td>Registration complete.</td></tr>
                            <?php
                            break;
                        case "incomplete": ?>
                            <tr><td>If an order was completed in CashNet but information did not pass correctly to the HCP order confirmation page, it will be listed as "incomplete." Please have a CINAB admin resolve this if necessary.</td></tr>
                            <?php
                            break;
                        case "cancel": ?>
                            <tr><td colspan="2">Order canceled. Please review instructions on <a href="/Finding_Voiding_Reversing_CASHNet_Transactions.pdf" target="_blank">how to issue a refund</a>.</td></tr>
                            <tr><td>Amount Refunded</td><td>$ <?php echo $order['refund_paid'] ?></td></tr>
                            <tr><td>Date Refunded</td><td><?php echo $order['refund_date'] ?></td></tr>
                            <tr><td>Refund Transaction</td><td><?php echo $order['refund_transaction_id'] ?></td></tr>
                            <?php
                            break;
                        case "refund": ?>
                            <tr><td colspan="2">Order canceled and refund has been issued. Please review instructions on <a href="/Finding_Voiding_Reversing_CASHNet_Transactions.pdf" target="_blank">how to issue a refund</a>.</td></tr>
                            <tr><td>Amount Refunded</td><td>$ <?php echo $order['refund_paid'] ?></td></tr>
                            <tr><td>Date Refunded</td><td><?php echo $order['refund_date'] ?></td></tr>
                            <tr><td>Refund Transaction</td><td><?php echo $order['refund_transaction_id'] ?></td></tr>
                            <?php
                            break;
                        default:
                            if ($order['status']) :
                                ?> <p>Status: <?php echo $order['status'] ?></p>
                                <p><input type="radio" name="status" value="<?php echo $order['status'] ?>" checked="checked" /><?php echo ucfirst($order['status']) ?></p>
                                <p><input type="radio" name="status" value="open" onclick="javascript:openThis();" />Re-open Order</p>
                                <p><input type="radio" name="status" value="cancel" onclick="javascript:cancelThis();" />Cancel Order</p>
                            <?php
                            else :
                                "<p>No registration status found. Please contact administrator.</p>";
                            endif;
                            break;
                    } ?>
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-6">
                            <button class="btn btn-primary btn-small" type="submit" data-toggle="modal" data-target="#updateStatusModal" id="updateStatusButton">Update Status</button>
                            <script>
                                $(document).ready(function(){
                                    $('#updateStatusButton').on("click",function(){
                                        // determine which status is being selected, then configure the status modal accordingly.
                                        var status = $('#order-status').val();
                                        $('#updateModalTitle').html(status);
                                        $('#updateStatusModal input[name=status]').val(status);
                                        var statusClass = status.replace(/ /g,"-");
                                        $('#updateStatusModal .form-group').addClass("hidden").find('input').prop("disabled","disabled"); // hide & disable all options
                                        $('#updateStatusModal').find('.'+statusClass).removeClass("hidden").find('input').prop("disabled",false); // show only appropriate options
                                    });
                                });
                            </script>
                        </div>
                    </div>
            </form>
        </div>
    </div>

    <!-- Modal: EDIT CONTACT INFO -->
    <div class="modal fade" id="editContact" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="editModalLabel">Edit Participant Contact Info</h4>
                </div>
                <div class="modal-body">
                    <form id="editShippingForm" class="form-horizontal" role="form" action="utils/process-form-action.php" method="post">
                        <input type="hidden" name="action" value="editContactInfo" />
                        <input type="hidden" name="callback" value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?orderNum='.$orderNum ?>" />
                        <input type="hidden" name="orderId" value="<?php echo $orderNum ?>" />

                        <?php
                        foreach ($participantKeys as $k => $v) :
                            ?>
                            <div class="form-group">
                                <label for="<?php echo $v ?>" class="col-sm-4 control-label"><?php echo $k ?></label>
                                <div class="col-sm-8"><input type="text" class="form-control" name="<?php echo $v ?>" value="<?php echo $order[$v] ?>" /></div>
                            </div>
                        <?php
                        endforeach;
                        ?>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="validateForm('#editShippingForm');">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- Mega-Modal: UPDATE STATUS -->
    <div class="modal fade" id="updateStatusModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="editModalLabel">Update Order Status to "<span id="updateModalTitle" style="text-transform:capitalize"></span>"</h4>
                </div>
                <div class="modal-body">
                    <form id="updateStatusForm" class="form-horizontal" role="form" action="utils/process-form-action.php" method="post">
                        <input type="hidden" name="action" value="updateRegStatus" />
                        <input type="hidden" name="callback" value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?orderNum='.$orderNum ?>" />
                        <input type="hidden" name="orderId" value="<?php echo $orderNum ?>" />
                        <input type="hidden" name="status" value="" />

                        <div class="form-group cancel refund">
                            <label class="col-sm-4 control-label">Amount Refunded</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control" name="amount_refunded" value="<?php echo $order['amount_refunded'] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group cancel refund">
                            <label class="col-sm-4 control-label">Date Refunded</label>
                            <div class="col-sm-8">
                                <div class="input-group date" id="ref-datepicker">
                                    <input type="text" class="form-control" id="datepicker" name="date_refunded" value="<?php echo $order['date_refunded'] ?>">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group cancel refund">
                            <label class="col-sm-4 control-label">Refund Transaction</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="" name="refund_tx" value="<?php echo $order['refund_tx'] ?>" placeholder="CashNet Transaction ID">
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="validateForm('#updateStatusForm');">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script type="text/javascript">
        $(function () {
            $('#ref-datepicker').datetimepicker({ format: 'YYYY-MM-DD' });
        });
    </script>

<?php /* SHOW ALL ORDERS */
else :
    ?>
    <p>No order specified.</p>


<?php endif; ?>

    <!-- end page content -->
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/footer.php'); ?>