<?php
$auth_realm = 'Order Admin Dashboard';
require_once 'auth.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $pageTitle ?> - Connectome In a Box Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" rel="stylesheet">
    <link href="/css/DT_bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
    <link href="/css/layout.css" rel="stylesheet">


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf-8" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8" language="javascript" src="/js/sortableDT/DT_bootstrap.js"></script>
    <script type="text/javascript" charset="utf-8" language="javascript" src="/js/functions.js"></script>
    <script type="text/javascript" charset="utf-8" language="JavaScript" src="/js/moment.js"></script>
    <script type="text/javascript" charset="utf-8" language="JavaScript" src="/js/bootstrap-datetimepicker.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<?php
require($_SERVER['DOCUMENT_ROOT'].'/includes/db-login.php');
require($_SERVER['DOCUMENT_ROOT'].'/includes/globals.php');
$pageName = $_SERVER['PHP_SELF'];
$pageName = preg_replace('(/(.*?).php(.*?))','$1',$pageName);
?>
<body>
<header id="cinab-main-nav">
    <div class="row">
        <?php if (!isset($_SESSION['login'])) : ?>
            <nav class="navbar navbar-default" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#cinab-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">XNAT Workshop Dashboard</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="cinab-navbar-collapse-1">
                    <ul class="nav navbar-right">
                        <button class="btn btn-reverse navbar-btn"><span class="glyphicon glyphicon-lock"></span> Admin Login</button>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>

        <?php else : ?>

            <nav class="navbar navbar-default" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#cinab-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/summary.php">XNAT Workshop Dashboard</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="cinab-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="<?php echo ($pageName == "registrations") ? "active" : ""; ?>"><a href="/workshops/index.php">Registration Dashboard</a></li>
                        <li class="<?php echo ($pageName == "xnat-downloads") ? "active" : ""; ?>"><a href="/xnat-downloads.php">XNAT Downloads</a></li>
                        <li><a href="?action=logOut">Log Out</a></li>
                    </ul>

                    <form class="navbar-form navbar-right" role="search" method="post" action="/utils/search-handler.php">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search by ID" name="search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div><!-- /.navbar-collapse -->
            </nav>
        <?php endif; ?>
    </div>
</header>

<div class="container" id="page-title">
    <h1><?php echo $pageTitle ?></h1>
</div>

<div class="container">
    <div id="primary-content">
        <?php if (!empty($_POST['alert'])) : $alerts = $_POST['alert']; ?>
            <?php foreach ($alerts as $key => $alert) : ?>
                <div class="alert <?php echo $alert ?> alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong><?php echo $_POST['alert-title'][$key] ?></strong> <?php echo stripslashes($_POST['alert-message'][$key]); ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
        <!-- end header.php -->
  
