<?php $pageTitle = "Search HCP Order Dashboard"; ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/header.php'); ?>
    <!-- page content goes here -->


<?php
printArray($_POST);

function printArray($array){
    echo "<ul>";
    foreach ($array as $key => $value){
        echo "<li>$key => $value</li>";
        if(is_array($value)){ //If $value is an array, print it as well!
            printArray($value);
        }
    }
    echo "</ul>";
}

?>

<?php

$alerts = array();
$callbackURL = "/index.php"; // default value
$searchTerm = $_POST['search'];

/*
 * based on search term, redirect user to one of four places:
 * 1. CINAB order page
 * 2. Course registration page
 * 3. Customer page (if customer ID found)
 * 4. Drive page (if serial number found)
 * 5. Home page with error message
*/
if ($searchTerm) :

    // search for order by order ID
    $q = "SELECT * FROM registrations WHERE ID = '".$searchTerm."' LIMIT 1;";
    $r = mysqli_fetch_array(mysqli_query($db,$q));
    if (count($r) > 0) :
        $callbackURL = "/view-registration.php?orderNum=".$r['ID'];
    else :
        // if no result came back from the mysqli query
        array_push( $alerts, array(
            "alert-title" => "No match found",
            "alert-message" => "Sorry, no customer, order ID, or drive was found matching ".$searchTerm."."
        ) );
    endif; // check for matching order ID

else :
    array_push( $alerts, array(
        "alert-title" => "No search term found",
        "alert-message" => "Please enter a search term."
    ) );
endif; // check for existence of search term

?>
    <form action="<?php echo $callbackURL ?>" method="post" id="callback-form">
        <?php foreach ($alerts as $key => $alert) : ?>
            <input type="hidden" name="alert[]" value="alert-info" />
            <input type="hidden" name="alert-title[]" value="<?php echo $alert['alert-title'] ?>: " />
            <textarea name="alert-message[]" class="form-control"><?php echo $alert['alert-message'] ?></textarea>
        <?php endforeach; ?>
    </form>

    <script>
        $(document).ready(function() {
            $('#callback-form').submit();
        });
    </script>

    <!-- end page content -->
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/footer.php'); ?>