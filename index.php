<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Connectome In A Box Fulfillment DB</title>
<script src="http://code.jquery.com/jquery-1.9.0.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<script type="text/javascript" src="/js/jquery.autotab.js"></script> <!-- autotab script via http://www.mathachew.com/sandbox/jquery-autotab/ -->

  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
  <script>
  $(function() {
    $( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
  });
  </script>

<style type="text/css">
	body {
		font-family: Calibri, Arial, Helvetica, sans-serif;
		font-size: 100%;
	}
	label {
		display:block;
		font-size:12px;
	}
	input.required {
		color:red;
		border:1px solid red;
	}
	.other-orders {
		background-color: #f7f7f7;
		float:left; 
		font-size: 12px;
		margin: 0 10px; 
		padding:0 10px; 
		width: 300px;
	}
	td, th {
		padding:0 10px;
	}
</style>
</head>

<body>



<?php 
include($_SERVER['DOCUMENT_ROOT'].'/includes/db-login.php');
?>
     

<?php

/* display order if correct info has been entered */

	
if (isset($_POST['orderNum']) && isset($_POST['email'])) :
	$orderNum = $_POST['orderNum']; 
	$orderEmail = $_POST['email'];
	
	$q = "SELECT *,DATE(timestamp) as order_date FROM registrations WHERE ID='".$orderNum."' AND contact_email='".$orderEmail."'";
	$orderCheck = mysqli_query($db,$q);
	
	if ($order = mysqli_fetch_array($orderCheck)) : 
		// display order info
		displayOrder($order); 
		echo "<hr size=\"1\" color=\"#ccc\" />";
		orderLookup(false);
	else : 
		// display error message 
		orderLookup(true); 
	endif; 

else: 
  // display order query form
  orderLookup(false);
endif; 
?>

<?php
function displayOrder($order) {
	global $db, $orderNum;

	?>
	<div id="orderInfo" style="padding:20px; position:relative;">

     	<h2>Workshop Registration ID: <?php echo $orderNum ?> / Status: <?php echo $order['status'] ?></h2>
        <p style="margin-bottom:30px;">Registration date: <?php echo $order['order_date']; ?></p>

        <table border="1" cellpadding="3" cellspacing="1">
        <tr><th>Customer info</th><th>Receipt</th><th>Status</th></tr>
        <tr valign="top"><td>
        	<p><?php echo $order['name']; ?> - <a href="mailto:<?php echo $order['contact_email'];?>"><?php echo $order['contact_email'];?></a></p>
            <p><?php echo $order['institution']; ?><br />
            <p><strong>Contact Phone:</strong></p>
            <p><?php echo ($order['contact_phone']) ? $order['contact_phone'] : "<span style='background:#fc9'>Error: no phone number specified. Please contact orders@humanconnectome.org to remedy.</span>"; ?></p>
        </td>

        <td>
        	<p>Registration Date: <?php echo $order['order_date'] ?></p>
            <p>Amount Paid: <?php echo $order['amount_paid'] ?></p>
            <p>Discount: <?php echo $order['discount'] ?></p>
            <?php if ($order['comments']) : ?>
                <hr size="1" />
                <p>Comments:</p>
                <p><em><?php echo $order['comments'] ?></em></p>
            <?php endif; ?>
        </td>

        <td>
        	<p>Status: <?php echo $order['status'] ?></p>
            <?php
			$status = $order['status'];
			switch ($status) {
				case "incomplete":
					?>
                    <p>Registration was not completed. Did you complete payment for this order? If so, please <a href="mailto:workshop@xnat.org">contact the administrator</a>.</p>
                    <?
					break;

				case "cancel":
					?>
                    <p>This registration has been canceled. If you have any concerns, please <a href="mailto:workshop@xnat.org">contact the administrator</a>.</p>
                    <?php
					break;

                case "refund":
                    ?>
                    <p>This registration has been canceled. If you have any concerns, please <a href="mailto:workshop@xnat.org">contact the administrator</a>.</p>
                    <p>Amount refunded: $<?php echo $order['refund_paid'] ?></p>
                    <p>Date refunded: <?php echo $order['refund_date'] ?></p>
                    <?php
                    break;

				case "complete":
					?>
					<p>This registration is complete .</p>
                    <?php if ($order['comments']) : ?>
                        <p><strong>Comments attached to :</strong></p>
                        <p><?php echo $order['notes'] ?></p>
                        <?php
						endif;
					break;

				default:
					echo "<p>No shipping status found. Please <a href=\"mailto:workshop@xnat.org\">contact the administrator</a>.</p>";
					break;
			}
			?>
        </td>
        </tr>
        </table>



     </div>

	<?php
}

?>



<?php 
function orderLookup($error) {
?>
<div style="font-size: 12px; padding: 2px 8px;">
	<div style="float:left; margin-right: 30px">
    <img src="/img/workshop-icon.png">
    </div>
	<?php if ($error) : ?>
    	<p><strong>No registration found.</strong></p>
    <?php endif; ?>
    <form action="/index.php" method="post">
         <h1>XNAT Workshop Registration Dashboard</h1>
         <p><strong>Look up a registration</strong></p>
         
         <label for="orderNum">Enter XNAT Registration ID</label>
         <input type="text" name="orderNum" />
         
         <label for="orderEmail">Enter the email of the registrant</label>
         <input type="text" name="email" />
         
         <input type="submit" value="Get order info" />
    </form>
    <p><a href="/workshops/index.php"><button>Log in as Administrator</button></a></p>
</div>
<?php
}
?>

<?php
mysqli_close($db);
?>

</body>
</html>