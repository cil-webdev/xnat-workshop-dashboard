<?php $pageTitle = "Update Database"; ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/header.php'); ?>
<!-- page content goes here -->


<?php 
printArray($_POST);

function printArray($array){
	echo "<ul>";
	 foreach ($array as $key => $value){
		echo "<li>$key => $value</li>";
		if(is_array($value)){ //If $value is an array, print it as well!
			printArray($value);
		}  
	} 
	echo "</ul>";
}

?>

<?php 

$alerts = array(); 
$action = $_POST['action'];
$callback = $_POST['callback'];

switch ($action) { 
	case "editContactInfo" :
        updateContactInfo($_POST['orderId']);
        break;
	case "updateRegStatus" :
        updateStatus($_POST['orderId']);
        break;
    case "addRegistration" :
        addRegistration();
        break;
    default :
		echo "<p class='color:red'>Sorry, I don't know what to do.</p>";
		break;
}

function submitQuery($q) { 
	global $db; 
	if (!mysqli_query($db,$q)) : 
		printf("<p>".$q."</p><p>Errormessage: %s</p><p><a href='javascript:history.back()'>Go back?</a><p>", mysqli_error($db)); die;  
	endif;
}

function updateContactInfo($orderId) {
    global $alerts;
    if ($orderId) :
        $q = "UPDATE registrations SET
			contact_name='".$_POST['name']."',
			contact_email='".$_POST['contact_email']."',
			contact_phone='".$_POST['contact_phone']."',
			institution='".$_POST['institution']."'
			WHERE ID='".$orderId."' ";
        submitQuery($q);
        array_push( $alerts, array(
            "alert-title" => "Database Updated",
            "alert-message" => $q
        ) );
    else :
        array_push( $alerts, array(
            "alert-title" => "No order ID found",
            "alert-message" => $q
        ) );
    endif;
}

function addRegistration(){
    global $db, $alerts;
    if ($_POST['contact_email'] && $_POST['contact_name'] && $_POST['event_id']) :
        $q = "INSERT INTO registrations (contact_name,contact_email,institution,event_id,status) 
                VALUES ('".$_POST['contact_name']."','".$_POST['contact_email']."','".$_POST['institution']."','".$_POST['event_id']."','complete') LIMIT 1;";
        submitQuery($q);
        array_push( $alerts, array(
            "alert-title" => "Registration Added",
            "alert-message" => $q
        ) );
    else :
        array_push($alerts, array(
            "alert-title" => "Could not add registration",
            "alert-message" => $q
        ));
    endif;
}

function updateStatus($orderId) { 
    global $db, $alerts;
	if ($orderId && $_POST['status']) :

        // handle requested status update
        $q = "UPDATE registrations SET status='".$_POST['status']."' ";
		$q .= ($_POST['amount_refunded']) ? ", refund_paid='".$_POST['amount_refunded']."' " : "";
		$q .= ($_POST['date_refunded']) ? ", refund_date='".$_POST['date_refunded']."' " : "";
		$q .= ($_POST['refund_tx']) ? ", refund_transaction_id='".$_POST['refund_tx']."' " : "";
		$q .="WHERE ID='".$orderId."';";
		submitQuery($q);   
		array_push( $alerts, array( 
			"alert-title" => "Database Updated", 
			"alert-message" => $q
			) );
	endif; 

}


?>
<form action="<?php echo $callback ?>" method="post" id="callback-form">
<input type="hidden" id="callback" name="callback" value="<?php echo $callback ?>" />
    <?php foreach ($alerts as $key => $alert) : ?>
        <input type="hidden" name="alert[]" value="alert-info" />
        <input type="hidden" name="alert-title[]" value="<?php echo $alert['alert-title'] ?>: " />
        <textarea name="alert-message[]" class="form-control"><?php echo $alert['alert-message'] ?></textarea>
    <?php endforeach; ?>
</form>

<script>
    var debugMode = false;
	$(document).ready(function() {
        if (debugMode) {
            var query = $('.query').val();
            var callback = $('#callback').val();
            var r = confirm("database updated. proceed to "+ callback + "?");
            if (r == true) {
                $('#callback-form').submit();
            }
        } else {
            $('#callback-form').submit();
        }
	});
</script>
            
<!-- end page content -->
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/footer.php'); ?>