<?php
/* 
 * assign global values, including SQL defined arrays 
 */
 
$drive_format = array(	"Linux",
						"Windows",
						"Mac");
$drive_status = array(	"new",
						"ready to ship",
						"hand delivered",
						"shipped",
						"returned to HCP",
						"defective",
						"returned to mfg",
						"removed",
						"source");	
$recycle_drive_status = array(	"Awaiting Inspection",
						"Inspected - Good",
						"Inspected - Bad");	
$release = 		array(	"HCP-2013Q1 Original",
						"HCP-2013Q1",
						"HCP-2013Q2",
						"HCP-2013Q3",
						"HCP-UR80",
						"HCP-UR100",
						"HCP-500S-1",
						"HCP-500S-2",
						"HCP-500S-3",
						"HCP-500S-4",
						"HCP-500S-5");
$order_status = array( 	"pending",
                        "open",
						"ready to ship", 
						"hand delivery", 
						"shipped",
						"complete",
						"cancel",
                        "refund");
$registration_status = array( 	"incomplete",
                        "open",
                        "complete",
                        "cancel",
                        "refund");
?>