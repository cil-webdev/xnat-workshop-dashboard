<?php $pageTitle = "Course Registration Dashboard"; ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/header.php'); ?>
    <!-- page content goes here -->

    <div class="container hidden">
        <?php
        printArray($_GET);

        function printArray($array){
            echo "<ul>";
            foreach ($array as $key => $value){
                echo "<li>$key => $value</li>";
                if(is_array($value)){ //If $value is an array, print it as well!
                    printArray($value);
                }
            }
            echo "</ul>";
        }

        ?>
    </div>


<?php /* SHOW ALL ORDERS */
// get all orders
$q = "SELECT *,DATE(timestamp) as order_date FROM orders WHERE (status != 'incomplete') AND (status != 'failed') AND (status != 'refund') AND (order_type = 'course') order by order_date ASC";
$order_list = mysqli_query($db,$q) or die($q);
?>

    <div class="row hidden">
    	<span id="filter-status-container"><label for="filter-status">Show Status:</label> 
        	<select name="filter-status" id="filter-status" class="form-control">
                <option value="">Show All</option>
                <?php foreach ($registration_status as $key => $value) : ?>
                    <option value="<?php echo strtoupper($value) ?>"><?php echo strtoupper($value) ?></option>
                <?php endforeach; ?>
            </select>
        </span>
    </div>

    <table class="table table-condensed table-hover" id="orderTable">
        <thead>
        <tr>
            <th>ID</th>
            <th id="orderStatus">Status</th>
            <th>Attendee</th>
            <th width="300">Institution</th>
            <th>Country</th>
            <th>Reg Date</th>
            <!-- <th>Course</th> -->
            <th>Rate</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($order_list) :
            while ($order = mysqli_fetch_array($order_list)) :

                ?>
                <tr>
                    <td id="<?php echo $order['id'] ?>">
                        <a href="/view-registration.php?orderNum=<?php echo $order['id'] ?>"><?php echo $order['id'] ?></a></td>
                    <td><?php echo strtoupper($order['status']) ?></td>
                    <td><?php echo $order['customer_name'] ?></td>
                    <td><?php echo $order['customer_institution'] ?></td>
                    <td><?php echo $order['billing_country'] ?></td>
                    <td><?php echo $order['order_date'] ?></td>
                    <!-- <td><?php echo $order['course_registered'] ?></td> -->
                    <td><?php echo $order['registration_type'] ?></td>
                </tr>
            <?
            endwhile;
        endif;
        ?>
        </tbody>
        <tfoot>
        <tr>
            <th colspan="7"></th>
        </tr>
        </tfoot>
    </table>

    <div class="hidden" id="show-all">
        <a href="/registrations.php">Show All Orders</a>
    </div>

    <div class="hidden">
        <form id="get-customer" action="/view-customer.php" method="post">
            <input type="hidden" id="customerID" name="customerID" value="" />
            <input type="hidden" id="customerEmail" name="customerEmail" value="" />
            <input type="hidden" id="customerName" name="customerName" value="" />
        </form>
    </div>

    <!--    <script src="/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script> -->
    <script>
        /* Initialize table and controls based on whether we show all records or not */
        var asInitVals = new Array();

        function statusFilter(opt){
            console.log("filtered on",opt);
            $('#orderTable').DataTable().column(1).search(opt).draw();
            $('#filter-status option[value="'+opt+'"]').prop("selected","selected"); // go back and make sure the filter menu is set if we call this function from inside the code.
        }

        $(document).ready(function() {
            // initialize data table
            var oTable = $('#orderTable').DataTable( {
                "iDisplayLength": 25
            } );

            // after table is created, add bootstrap standard classes to controls.
            $(".dataTables_filter label input").addClass('form-control');
            $(".dataTables_length label select").addClass('form-control');

            $("#filter-status").on( "change",function(){
                statusFilter( $(this).val() )
            });

        } );

        // HACK: move table filter menu into header, after table has been fully initialized
        $('#orderTable').on("init.dt",function(){
            console.log("complete!");
            $('#orderTable_filter').append( $('#filter-status-container') );
            statusFilter("OPEN");
        });
    </script>

    <!-- end page content -->
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/footer.php'); ?>