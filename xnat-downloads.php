<?php $pageTitle = "XNAT Downloads"; ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/header.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'].'/includes/db-login-downloads.php'); ?>
    <!-- page content goes here -->

    <div class="container hidden">
        <?php
        printArray($_GET);

        function printArray($array){
            echo "<ul>";
            foreach ($array as $key => $value){
                echo "<li>$key => $value</li>";
                if(is_array($value)){ //If $value is an array, print it as well!
                    printArray($value);
                }
            }
            echo "</ul>";
        }

        ?>
    </div>


<?php /* SHOW ALL DOWNLOADS */
// get all orders
$q = "SELECT *,DATE(timestamp) as download_date FROM downloads order by timestamp DESC";
$downloads = mysqli_query($db,$q) or die($q);
?>

    <table class="table table-condensed table-hover" id="orderTable">
        <thead>
        <tr>
            <th width="110">Date</th>
            <th>Name</th>
            <th>Email</th>
            <th width="300">Institution</th>
            <th>DUT</th>
            <th>On Mailing List?</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($downloads) :
            while ($download = mysqli_fetch_array($downloads)) :

                ?>
                <tr>
                    <td id="<?php echo $download['timestamp'] ?>"><?php echo $download['download_date'] ?></td>
                    <td><?php echo $download['name'] ?></td>
                    <td><?php echo $download['email'] ?></td>
                    <td><?php echo $download['institution'] ?></td>
                    <td><?php if ($download['DUT']) { echo "<span class='glyphicon glyphicon-ok'></span>"; } ?></td>
                    <td><?php if ($download['mailing_list']) { echo "<span class='glyphicon glyphicon-ok'></span>"; } ?></td>
                </tr>
            <?
            endwhile;
        endif;
        ?>
        </tbody>
        <tfoot>
        <tr>
            <th colspan="7"></th>
        </tr>
        </tfoot>
    </table>



    <!--    <script src="/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script> -->
    <script>
        /* Initialize table and controls based on whether we show all records or not */
        var asInitVals = new Array();

        function statusFilter(opt){
            $('#orderTable').DataTable().column(1).search(opt).draw();
        }

        $(document).ready(function() {
            // initialize data table
            var oTable = $('#orderTable').DataTable( {
                "iDisplayLength": 100
            } );

            // after table is created, add bootstrap standard classes to controls.
            // $(".dataTables_filter label input").addClass('form-control');
            $(".dataTables_length").find("select").addClass('form-control');

            /*
            $("#filter-status").on( "change",function(){
                statusFilter( $(this).val() )
            });
            */

        } );

        // HACK: move table filter menu into header, after table has been fully initialized
        /*
        $('#orderTable').on("init.dt",function(){
            $('#orderTable_filter').append( $('#filter-status-container') );
            statusFilter("OPEN");
        });
        */
    </script>

    <!-- end page content -->
<?php require($_SERVER['DOCUMENT_ROOT'].'/template/footer.php'); ?>